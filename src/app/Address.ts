export interface Address {
    city: string;
    address1: String;
    address2: String;
    portalCode: number;
}