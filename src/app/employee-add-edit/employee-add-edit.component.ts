import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-employee-add-edit',
  templateUrl: './employee-add-edit.component.html',
  styleUrls: ['./employee-add-edit.component.css']
})
export class EmployeeAddEditComponent implements OnInit {
  public employees = [];
  public displayEditView = false;

  public headElements = ["ID", "Name", "phone No.", "City", "Address1", "Address2", "Portal code", "Edit"]



  constructor(private _employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this._employeeService.getEmployees().
      subscribe(data => this.employees = data);
  }

  checkPhoneNo(phoneNo) {
    return /^\d+$/.test(phoneNo);
  }

  edit(id) {
  //  debugger;

    this.router.navigate(['/edit',{ id: id }]);

    this.displayEditView = true;

    console.log(id);
  }

  addNew(){
    this.router.navigate(['/emplyoees/add']);
  }

}
