import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-task1',
  templateUrl: './task1.component.html',
  styleUrls: ['./task1.component.css']
})
export class Task1Component implements OnInit {
  public employees = [];
  
  //Table headings
  public headElements = ["ID", "Name", "phone No.", "City", "Address1", "Address2", "Portal code"]


  constructor(private _employeeService: EmployeeService) { }

  ngOnInit(): void {
    this._employeeService.getEmployees().
      subscribe(data => this.employees = data);
  }

  //function to validate Check phone no
  checkPhoneNo(phoneNo) {
    return /^\d+$/.test(phoneNo);
  }

}
