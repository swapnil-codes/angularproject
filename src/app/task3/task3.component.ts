import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task3',
  templateUrl: './task3.component.html',
  styleUrls: ['./task3.component.css']
})
export class Task3Component implements OnInit {
  public employees = [];
  public headElements = ["ID", "Name", "phone No.", "City", "Address1", "Address2", "Portal code", "Action"]

  constructor(private _employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this._employeeService.getEmployees().
      subscribe(data => this.employees = data);
  }
  checkPhoneNo(phoneNo) {
    return /^\d+$/.test(phoneNo);
  }

}
