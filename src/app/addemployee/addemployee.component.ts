import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../Employee';

@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./addemployee.component.css']
})
export class AddemployeeComponent implements OnInit {

  public employeedata=new Employee();
  constructor(private _employeeService: EmployeeService) { }

  ngOnInit(): void {
  }

  addEmployee(emplyoee){
    debugger;
    this._employeeService.addEmployee(emplyoee).subscribe(
data=>console.log("sucess!",data),
error=>console.log("error!",error)
    )

  }

}
