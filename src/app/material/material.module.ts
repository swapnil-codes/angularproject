import { NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
const MaterialComponent=[MatSliderModule]
@NgModule({
  declarations: [],
  imports: [MaterialComponent],
  exports: [MaterialComponent]

})
export class MaterialModule { }
