import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  public employees = [];


  public headElements = ["ID", "Name", "phone No.","City", "Address1", "Address2","Portal code"]

  constructor(private _employeeService: EmployeeService) { }

  ngOnInit(): void {

    this._employeeService.getEmployees().
      subscribe(data => this.employees = data);

    //   this.employees=this._employeeService.getEmployees();
    console.log(this.employees);



  }

  checkPhoneNo(phoneNo) {
    
    return /^\d+$/.test(phoneNo);
  }
}
