import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-task2',
  templateUrl: './task2.component.html',
  styleUrls: ['./task2.component.css']
})
export class Task2Component implements OnInit {
  public employees = [];

  public headElements = ["ID", "Name", "phone No.", "City", "Address1", "Address2", "Portal code"]
  
  noRecordFounds: string = '';


  constructor(private _employeeService: EmployeeService) { }

  ngOnInit(): void {
    debugger;
    this._employeeService.getEmployees().
      subscribe(data => this.employees = data);
  }

  checkPhoneNo(phoneNo) {
    return /^\d+$/.test(phoneNo);
  }

 //filter function to sort records based on name and city 
  filter(value) {
    value = value.toLowerCase();

    this.employees = this.employees.filter(function (employee) {
      return (employee.name.toLowerCase().indexOf(value) >= 0) || (employee.city.toLowerCase().indexOf(value) >= 0);
    });

    //Added check to give "No record available" message
    if (this.employees.length == 0) {
      this.noRecordFounds = "No record available";
    }
  }

//function to refresh UI
  refresh() {
    this.noRecordFounds = '';
    this._employeeService.getEmployees().
      subscribe(data => this.employees = data);
}

}
